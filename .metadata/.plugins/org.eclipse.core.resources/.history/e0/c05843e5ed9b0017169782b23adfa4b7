#include "linkedlist.h"

void ll_init(struct linkedList*);
bool ll_add(struct linkedList* , const void* , uint32_t );
bool ll_addIndex(struct linkedList*, const void*, uint32_t, uint32_t);
bool ll_remove(struct linkedList*, uint32_t);
void* ll_get(struct linkedList*, uint32_t);
void ll_clear(struct linkedList*);
uint32_t ll_size(struct linkedList*);
struct linkedListIterator* ll_getIterator(struct linkedList*);
bool ll_hasNext(struct linkedListIterator*);
void* ll_next(struct linkedListIterator*);

//private functions
static struct listNode* initNode();
static struct listNode* ll_nextNode(struct linkedListIterator* iter);

void ll_init(struct linkedList* list){
	if(list != NULL){
		list -> head = NULL;
		list -> tail = NULL;
		list -> size = 0;
	}
}

/**
 * This function adds a new node to the linked list. The size of the list is updates, and references between nodes are
 * updated if necessary. This function will handle all memory checks as well to ensure safe data allocation.
 * @param list		pointer to a LinkedList structure.
 * @param object	pointer to and object of any type.
 * @param size		size of the object that the object pointer points to.
 * @return			true if the object is successfully added to the list, false otherwise.
 */
bool ll_add(struct linkedList* list, const void* object, uint32_t size) {
	if (list != NULL && object != NULL && size > 0) {
		struct listNode* node = initNode();
		void* objData = malloc(size);

		//ensure there was enough memory to create a node and copy the object data.
		if (node != NULL && objData != NULL) {
			node->data = memcpy(objData, object, size);
			node->dataSize = size;

			//Need to change the next reference for the previous node if the size of the list is greater than 0
			if (ll_size(list) > 0) {
				struct listNode* prevNode = list->tail;
				prevNode->nextNode = node;
				node->prevNode = prevNode;

			}else{
				list->head = node;
			}
			list->tail = node;
			list->size++;

			return true;
		}

	}

	return false;
}

/**
 * This function will add a new node to the given linkedList containing the given data to the given index.
 * @param list		A non-null pointer to the linkedList that will hold the new node.
 * @param object	A non-null pointer to the data the new node will contain.
 * @param size		The size of the data object.
 * @param index		The index to insert the new data at within the linkedList.
 * @return			true is returned if the insertion was successful, false otherwise.
 */
bool ll_addIndex(struct linkedList* list, const void* object, uint32_t size, uint32_t index){
	if(list != NULL && object != NULL && size > 0 && index >= 0 && index < ll_size(list)){
		struct listNode* node = initNode();
		void* nodeData = malloc(size);

		if(node != NULL && nodeData != NULL){
			node -> data = memcpy(nodeData, object, size);
			node->dataSize = size;

			if(ll_size(list) == 0){
				list->head = node;
				list->tail = node;
			}else if(index == 0){
				node->nextNode = list->head;
				list->head = node;
			}else if(index == ll_size(list) - 1){
				node->prevNode = list->tail;
				list->tail = node;
			}else{
				struct listNode* movNode = ll_get(list, index);
				movNode->prevNode->nextNode = node;
				node->prevNode = movNode->prevNode;

				node->nextNode = movNode;
				movNode->prevNode = node;

			}
			list->size++;
			return true;

		}
	}
	return false;
}

/**
 * This function will remove the node at a given index from a given linkedList. This function frees all memory
 * associated with the node to be removed, adjusts all node references for the linkedList, and decrements the
 * size of the linkedList.
 * @param list		A non-null pointer to the list that the node will be removed from.
 * @param index		The index of the linkedList that the node will be removed from.
 * @return			true is returned if the node is successfully returned, false otherwise.
 */
bool ll_remove(struct linkedList* list, uint32_t index){
	if(list != NULL && index >= 0 && index < ll_size(list)){
		struct listNode* rmvNode = ll_get(list, index);

		//Must change references of the next node and previous node to ensure there is no break in the linked list
		rmvNode->prevNode->nextNode = rmvNode->nextNode;
		rmvNode->nextNode->prevNode = rmvNode->prevNode;

		//If the node to be removed is the head or the tail of the linkedList, then the reference to the head of tail must be changed.
		if(index == 0)
			list->head = rmvNode->nextNode;
		if(index == ll_size(list) - 1)
			list->tail = rmvNode->prevNode;
		list->size--;

		//Must ensure the data of the node is freed before the node to ensure memory used by data is not lost.
		free(rmvNode->data);
		free(rmvNode);

		return true;
	}
	return false;
}

/**
 * This function will get the pointer to data for the node of a given linkedList at a given index.
 * @param list		A non-null pointer to a linkedList.
 * @param index		The index of the node that data will be retrieved from.
 * @return			A pointer to the node's data will be returned if successful, NULL is returned otherwise.
 */
void* ll_get(struct linkedList* list, uint32_t index){
	if(list != NULL && index >= 0 && index < ll_size(list)){
		struct linkedListIterator* iterator = ll_getIterator(list);
		int index = 0;

		for(int i = 0; i < index; i++){
			if(ll_hasNext(iterator))
				ll_next(iterator);
		}

		void* data = ll_next(iterator);

		//Iterator is no longer needed, so its memory should be returned to the system.
		free(iterator);

		return data;
	}
	return NULL;
}

/**
 * This function will clear all the data from a given linkedList and reset the list to initialized values.
 * @param list	A non-null pointer to a linkedList.
 */
void ll_clear(struct linkedList* list){
	if(list != NULL){
		struct linkedListIterator* iterator = ll_getIterator(list);
		while(ll_hasNext(iterator)){
			ll_remove(list,0);
		}
	}
}

uint32_t ll_size(struct linkedList* list){
	if(list != NULL){
		return list->size;
	}

	return 0;
}

/**
 * This function will create an iterator for a provided linkedList. Upon creation of an iterator, the head will
 * point to the head of the list. To obtain data from the head of the list, ll_next() should be called.
 * @param list	A non-null pointer to a list structure.
 * @return		A pointer to a linkedList iterator will be returned if successful. If unsuccessful, null is returned.
 */
struct linkedListIterator* ll_getIterator(struct linkedList* list){
	if(list != NULL){
		struct linkedListIterator* iterator = malloc(sizeof(struct linkedListIterator));
		iterator->current = list->head;
		return iterator;
	}
	return NULL;
}

/**
 * This function will indicate if ll_next() can be called on the given iterator.
 * @param iter	A non-null pointer to a iterator structure.
 * @return		true if ll_next() can be called on the iterator, false otherwise.
 */
bool ll_hasNext(struct linkedListIterator* iter){
	if(iter != NULL){
		return iter->current != NULL;
	}
	return false;
}

/**
 * This function will return the data stored in the next node of the linkedList if it exists.
 * @param iter	A non-null pointer to an iterator for a linkedList.
 * @return		A pointer to the object data of the next node if it exists; false otherwise.
 */
void* ll_next(struct linkedListIterator* iter){
	//ensure that iter is not null and it has a next element.
		if(ll_hasNext(iter)){
			struct listNode* current = iter->current;
			iter->current = current->nextNode;
			return current->data;
		}
		return NULL;
}

static struct listNode* initNode(){
	struct listNode* tempNode = malloc(sizeof(struct listNode));

	//if there is note enough memory for a new Node, return the null pointer before any assignment
	if(tempNode == NULL)
		return tempNode;

	tempNode->data = NULL;
	tempNode->dataSize = 0;
	tempNode->nextNode = NULL;
	tempNode->prevNode = NULL;

	return tempNode;
}

static struct listNode* ll_nextNode(struct linkedListIterator* iter){
	//ensure that iter is not null and it has a next element.
		if(ll_hasNext(iter)){
			struct listNode* current = iter->current;
			iter->current = current->nextNode;
			return current;
		}
		return NULL;
}

static struct listNode* ll_getNode(struct linkedList* list, uint32_t index){
	if(list != NULL && index >= 0 && index < ll_size(list)){
		struct linkedListIterator* iterator = ll_getIterator(list);
		int index = 0;

		for(int i = 0; i < index; i++){
			if(ll_hasNext(iterator))
				ll_nextNode(iterator);
		}

		void* data = ll_nextNode(iterator);

		//Iterator is no longer needed, so its memory should be returned to the system.
		free(iterator);

		return data;
	}
	return NULL;
}


