#include "linkedlist.h"
#include <string.h>
#include <stdlib.h>

void ll_init(struct linkedList* list) {
	if (list != NULL) {
		list->head = NULL;
		list->tail = NULL;
		list->size = 0;
	}
}

bool ll_add(struct linkedList* list, const void* object, uint32_t size) {
	bool retVal = false;
	// Check to make certain list is not NULL and the object is not null.
	if ((list != NULL) && (object != NULL) && (size > 0)) {
		// Malloc space for the node.
		struct listNode* n = malloc(sizeof(struct listNode));

		void* data = malloc(size);

		// Check and make certain the items are not NULL.
		// If they are NULL, we can not do anything with them.

		if ((data != NULL) && (n != NULL)) {
			// Copy the data using memcpy
			memcpy(data, object, size);

			// Assign the pointer to the data in the element.
			n->data = data;
			n->dataSize = size;
			n->nextNode = NULL;
			n->prevNode = NULL;

			// Check and handle adding to empty list.
			if (list->tail == NULL) {
				// Set up the head and tail to point to this data.
				list->head = n;
				list->tail = n;
				// Set up the size to be one greater (really, zero.)
				list->size++;
			} else {
				// We are adding to a non-empty list.
				// Nothing changes.
				n->prevNode = list->tail;
				list->tail->nextNode = n;
				list->tail = n;
				list->size++;
			}
			retVal = true;
		}
	}
	return retVal;
}

void* ll_get(struct linkedList* list, uint32_t index) {
	void* retVal = NULL;
	if (list != NULL) {
		// Start by checking the range on the index.
		if (index < list->size) {
			struct listNode* element = list->head;
			uint32_t count = 0;

			while (count < index) {
				element = element->nextNode;
				count++;
			}
			retVal = element->data;
		}
	}
	return retVal;
}

bool ll_remove(struct linkedList* list, uint32_t index) {
	bool retVal = false;
	if (list != NULL) {
		if (index < list->size) {
			// Index is such that we can remove something.

			// Special cases.
			if (list->size == 1) {
				// We are effectivly removing the head and the tail.
				struct listNode* element = list->head;
				free(element->data);
				free(element);
				list->head = NULL;
				list->tail = NULL;
				list->size = 0;
			} else if (index == ((list->size) - 1)) {
				// Here we are removing the tail of the list.
				struct listNode* element = list->tail;
				list->tail = element->prevNode;
				list->tail->nextNode = NULL;
				free(element->data);
				free(element);
				list->size--;
			} else if (index == 0) {
				// Here we are removing the head of the list.
				struct listNode* element = list->head;
				list->head = element->nextNode;
				list->head->prevNode = NULL;
				free(element->data);
				free(element);
				list->size--;
			} else {
				struct listNode* element = list->head;
				struct listNode* prev = NULL;
				uint32_t count = 0;

				while (count < index) {
					prev = element;
					element = element->nextNode;
					count++;
				}
				//printf("%x %x %x\n", prev, element, list->head);
				prev->nextNode = element->nextNode;
				element->nextNode->prevNode = element->prevNode;
				free(element->data);
				free(element);
				list->size--;
			}
			retVal = true;
		}
	}
	return retVal;
}

struct linkedListIterator* ll_getIterator(struct linkedList* list) {
	struct linkedListIterator* retVal = NULL;
	if (list != NULL) {
		retVal = malloc(sizeof(struct linkedListIterator));
		retVal->current = list->head;
	}
	return retVal;

}

bool ll_hasNext(struct linkedListIterator* iter) {
	bool retVal = false;
	if (iter != NULL) {
		if (iter->current != NULL) {
			retVal = true;
		}
	}
	return retVal;
}

void* ll_next(struct linkedListIterator* iter) {
	void* retVal = NULL;
	if (iter != NULL) {
		if (iter->current != NULL) {
			retVal = iter->current->data;

			// Increment the location in the list.
			iter->current = iter->current->nextNode;
		}
	}
	return retVal;
}

bool ll_addIndex(struct linkedList* list, const void* object, uint32_t size,
		uint32_t index) {
	bool retVal = false;

	if ((list != NULL) && (object != NULL) && (size != 0)) {
		// Start by checking to make certain the object is not null
		// and the list itself is not NULL.

		struct listNode* n = malloc(sizeof(struct listNode));

		void* data = malloc(size);

		// Check and make certain the items are not NULL.
		// If they are NULL, we can not do anything with them.

		if ((data != NULL) && (n != NULL)) {
			// Copy the data using memcpy
			memcpy(data, object, size);

			// Assign the pointer to the data in the element.
			n->data = data;
			n->dataSize = size;
			n->nextNode = NULL;
			n->prevNode = NULL;

			if (index == (list->size)) {
				// Special case in that we are adding to the tail.
				// Simply call the base add method.
				retVal = ll_add(list, object, size);

			} else if (index == 0) {
				// Special case in that we are adding to the head.
				n->nextNode = list->head;
				list->head->prevNode = n;
				list->head = n;
				list->size++;
				retVal = true;
			}

			else if (index < (list->size)) {
				// Non-special case of adding in the middle of the list.
				uint32_t count = 0;
				struct listNode* insertionPoint = list->head;
				// Walk the list until we get to the appropriate location.
				while (count < index) {
					count++;
					insertionPoint = insertionPoint->nextNode;
				}

				// At the appropriate spot.
				n->prevNode = insertionPoint->prevNode;
				n->nextNode = insertionPoint;
				n->prevNode->nextNode = n;
				insertionPoint->prevNode = n;
				list->size++;
				retVal = true;
			} else {
				// The index is out of range.
				free(data);
				free(n);
			}
		}
	}
	return retVal;
}

void ll_clear(struct linkedList* list) {
	if (list != NULL) {
		struct listNode *nextNode = NULL;
		struct listNode *thisNode = list->head;

		while (thisNode != NULL) {
			nextNode = thisNode->nextNode;
			free(thisNode->data);
			free(thisNode);
			thisNode = nextNode;
			list->size--;
		}
		list->head = NULL;
		list->tail = NULL;
	}
	list->size = 0;
}

uint32_t ll_size(struct linkedList* list) {
	uint32_t retVal = 0;
	if (list != NULL) {
		retVal = list->size;
	}
	return retVal;
}
