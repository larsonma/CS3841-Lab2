/**
 * This file contains the implementation of a linkedList data structure. Several functions are provided below which
 * allow for creation, manipulation, and deletion of nodes and linkedLists. A few private functions are provided
 * as well.
 * @file linkedlist.c
 * @author Mitchell Larson
 * @date 9/17/2017
 */
#include "linkedlist.h"

void ll_init(struct linkedList*);
bool ll_add(struct linkedList* , const void* , uint32_t );
bool ll_addIndex(struct linkedList*, const void*, uint32_t, uint32_t);
bool ll_remove(struct linkedList*, uint32_t);
void* ll_get(struct linkedList*, uint32_t);
void ll_clear(struct linkedList*);
uint32_t ll_size(struct linkedList*);
struct linkedListIterator* ll_getIterator(struct linkedList*);
bool ll_hasNext(struct linkedListIterator*);
void* ll_next(struct linkedListIterator*);

//private functions
static struct listNode* initNode();
static struct listNode* ll_getNode(struct linkedList* list, uint32_t index);

/**
 * This function accepts a linkedList pointer and initializes the state of the linkedList. This should not
 * be called without a prior call to ll_clear() if the list has already been initialized.
 * @param list	A non-null pointer to a linkedList structure.
 */
void ll_init(struct linkedList* list){
	if(list != NULL){
		list -> head = NULL;
		list -> tail = NULL;
		list -> size = 0;
	}
}

/**
 * This function adds a new node to the linked list. The size of the list is updates, and references between nodes are
 * updated if necessary. This function will handle all memory checks as well to ensure safe data allocation.
 * @param list		pointer to a LinkedList structure.
 * @param object	pointer to and object of any type.
 * @param size		size of the object that the object pointer points to.
 * @return			true if the object is successfully added to the list, false otherwise.
 */
bool ll_add(struct linkedList* list, const void* object, uint32_t size) {
	if (list != NULL && object != NULL && size > 0) {
		struct listNode* node = initNode();
		void* objData = malloc(size);

		//ensure there was enough memory to create a node and copy the object data.
		if (node != NULL && objData != NULL) {
			node->data = memcpy(objData, object, size);
			node->dataSize = size;

			//Need to change the next reference for the previous node if the size of the list is greater than 0.
			if (ll_size(list) > 0) {
				struct listNode* prevNode = list->tail;
				prevNode->nextNode = node;
				node->prevNode = prevNode;

			}else{
				//If the list is empty, then the list's head must be set to the node.
				list->head = node;
			}
			list->tail = node;
			list->size++;

			return true;
		}

	}

	return false;
}

/**
 * This function will add a new node to the given linkedList containing the given data to the given index.
 * @param list		A non-null pointer to the linkedList that will hold the new node.
 * @param object	A non-null pointer to the data the new node will contain.
 * @param size		The size of the data object.
 * @param index		The index to insert the new data at within the linkedList.
 * @return			true is returned if the insertion was successful, false otherwise.
 */
bool ll_addIndex(struct linkedList* list, const void* object, uint32_t size, uint32_t index){
	if(list != NULL && object != NULL && size > 0 && index >= 0 && index <= ll_size(list)){
		//if the node is being added to the end of the list or list is empty, it is the equivalent of calling ll_add()
		if(index == ll_size(list) || ll_size(list) == 0)
			return ll_add(list, object, size);

		struct listNode* node = initNode();
		void* nodeData = malloc(size);

		//Ensure that the node and the nodeData had their data properly allocated.
		if(node != NULL && nodeData != NULL){
			node -> data = memcpy(nodeData, object, size);
			node->dataSize = size;

			//If the destination index is 0, the head of the list must be changed, and only references for the next node must be changed
			//The case where the destination index is not the head requires extra node reference changes and doesn't involve the head.
			if(index == 0){
				node->nextNode = list->head;
				node->nextNode->prevNode = node;
				list->head = node;
			}else{
				struct listNode* movNode = ll_getNode(list, index);
				movNode->prevNode->nextNode = node;
				node->prevNode = movNode->prevNode;

				node->nextNode = movNode;
				movNode->prevNode = node;

			}
			list->size++;
			return true;

		}
	}
	return false;
}

/**
 * This function will remove the node at a given index from a given linkedList. This function frees all memory
 * associated with the node to be removed, adjusts all node references for the linkedList, and decrements the
 * size of the linkedList.
 * @param list		A non-null pointer to the list that the node will be removed from.
 * @param index		The index of the linkedList that the node will be removed from.
 * @return			true is returned if the node is successfully returned, false otherwise.
 */
bool ll_remove(struct linkedList* list, uint32_t index){
	if(list != NULL && index >= 0 && index < ll_size(list)){
		struct listNode* rmvNode = ll_getNode(list, index);

		//Must change references of the next node and previous node to ensure there is no break in the linked list
		if(rmvNode->prevNode != NULL)
			rmvNode->prevNode->nextNode = rmvNode->nextNode;
		if(rmvNode->nextNode != NULL)
			rmvNode->nextNode->prevNode = rmvNode->prevNode;

		//If the node to be removed is the head or the tail of the linkedList, then the reference to the head of tail must be changed.
		if(index == 0)
			list->head = rmvNode->nextNode;
		if(index == ll_size(list) - 1)
			list->tail = rmvNode->prevNode;
		list->size--;

		//Must ensure the data of the node is freed before the node to ensure memory used by data is not lost.
		free(rmvNode->data);
		free(rmvNode);


		return true;
	}
	return false;
}

/**
 * This function will get the pointer to data for the node of a given linkedList at a given index.
 * @param list		A non-null pointer to a linkedList.
 * @param index		The index of the node that data will be retrieved from.
 * @return			A pointer to the node's data will be returned if successful, NULL is returned otherwise.
 */
void* ll_get(struct linkedList* list, uint32_t index){
	if(list != NULL && index >= 0 && index < ll_size(list)){
		struct linkedListIterator* iterator = ll_getIterator(list);

		//Iterate though the list until the index before the requested node so that the data can be grabbed.
		for(int i = 0; i < index; i++){
			if(ll_hasNext(iterator))
				ll_next(iterator);
		}

		//Assign the next data to a variable so that the iterator can be freed prior to the function return.
		void* data = ll_next(iterator);

		//Iterator is no longer needed, so its memory should be returned to the system.
		free(iterator);

		return data;
	}
	return NULL;
}

/**
 * This function will clear all the data from a given linkedList and reset the list to initialized values.
 * @param list	A non-null pointer to a linkedList.
 */
void ll_clear(struct linkedList* list){
	if(list != NULL){
		struct linkedListIterator* iterator = ll_getIterator(list);

		//Remove the 0 index of the list so that the front of the linked list can be repeatedly removed by shifting left.
		while(ll_hasNext(iterator)){
			ll_next(iterator);
			ll_remove(list,0);
		}

		//Return the iterator memory to the system.
		free(iterator);
	}
}

/**
 * This function retrieves the size of a given linked list.
 * @param list	A non-null pointer to a linkedList structure.
 * @return		The size of the list. If a list pointer is null, 0 is returned.
 */
uint32_t ll_size(struct linkedList* list){
	if(list != NULL){
		return list->size;
	}

	return 0;
}

/**
 * This function will create an iterator for a provided linkedList. Upon creation of an iterator, the head will
 * point to the head of the list. To obtain data from the head of the list, ll_next() should be called.
 * @param list	A non-null pointer to a list structure.
 * @return		A pointer to a linkedList iterator will be returned if successful. If unsuccessful, null is returned.
 */
struct linkedListIterator* ll_getIterator(struct linkedList* list){
	if(list != NULL){
		struct linkedListIterator* iterator = malloc(sizeof(struct linkedListIterator));

		//Assign the head of the linked list to current of the iterator to start before index 0. This allows index 0 to be
		//retrieved with a call to ll_next().
		iterator->current = list->head;
		return iterator;
	}
	return NULL;
}

/**
 * This function will indicate if ll_next() can be called on the given iterator.
 * @param iter	A non-null pointer to a iterator structure.
 * @return		true if ll_next() can be called on the iterator, false otherwise.
 */
bool ll_hasNext(struct linkedListIterator* iter){
	if(iter != NULL){
		//Ensure that the next object exists.
		return iter->current != NULL;
	}
	return false;
}

/**
 * This function will return the data stored in the next node of the linkedList if it exists.
 * @param iter	A non-null pointer to an iterator for a linkedList.
 * @return		A pointer to the object data of the next node if it exists; false otherwise.
 */
void* ll_next(struct linkedListIterator* iter){
	//ensure that iter is not null and it has a next element.
		if(ll_hasNext(iter)){
			struct listNode* current = iter->current;

			//Move the iterator by assigning current to the next node.
			iter->current = current->nextNode;
			return current->data;
		}
		return NULL;
}

/**
 * This is a private function that is used to initialize a list node.
 * @return	A pointer to a new initialized list node.
 */
static struct listNode* initNode(){
	struct listNode* tempNode = malloc(sizeof(struct listNode));

	//if there is note enough memory for a new Node, return the null pointer before any assignment
	if(tempNode == NULL)
		return tempNode;

	tempNode->data = NULL;
	tempNode->dataSize = 0;
	tempNode->nextNode = NULL;
	tempNode->prevNode = NULL;

	return tempNode;
}

/**
 * This function returns the node at a given index. This function is meant for private internal use.
 * The functionality is similar to ll_get(), but instead returns the entire node rather than the
 * object data.
 * @param list		A non-null pointer to a linked list.
 * @param index		The index to retrieve a node from.
 * @return			The node at a given index of a given linked list.
 */
static struct listNode* ll_getNode(struct linkedList* list, uint32_t index){
	if(list != NULL && index >= 0 && index < ll_size(list)){
		struct linkedListIterator* iterator = ll_getIterator(list);

		for(int i = 0; i < index; i++){
			if(ll_hasNext(iterator))
				ll_next(iterator);
		}

		struct listNode* node = iterator->current;

		//Iterator is no longer needed, so its memory should be returned to the system.
		free(iterator);

		return node;
	}
	return NULL;
}


